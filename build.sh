#!/bin/bash

set -e

TAG=latest

### FUNCTIONS ###
# Build a docker image
function docker_build() {
    local name=$1
    local prefix=$2
    local image=vpemfh7/$prefix-$name:$TAG
    cd $([ -z "$3" ] && echo "$prefix/$name" || echo "$3")
    echo "-------------------------" building image $image in $(pwd)
    docker build --rm -t $image .
    cd -
}

function build_all() {
    local flag=$1
    local base_dir=$2
    local prefix=$base_dir
    
    if [[ $flag == "-S" ]]; then
        echo "Building subdirectories"
        for dir in $(ls $base_dir); do
            docker_build $dir $prefix
        done
    else
        docker_build $1
    fi
}


function build_deps() {
    echo "Building perf ---------------------------------------------------------"
    sudo apt install linux-tools-common linux-tools-generic linux-tools-`uname -r` -y
    sudo sh -c 'echo -1 >/proc/sys/kernel/perf_event_paranoid'
    
    echo "Building collectl -----------------------------------------------------"
    tar -xzf collectl-4.3.1.src.tar.gz
    cd collectl-4.3.1
    sudo sh ./INSTALL
    cd ..
    
    echo "Building pssh -----------------------------------------------------"
    sudo apt install pssh -y
    
    if command -v jtm &> /dev/null
    then
        echo jtm Already Exist!
    else
        echo "Building JTM ----------------------------------------------------------"
        git clone https://github.com/ldn-softdev/jtm.git
        cd jtm
        c++ -o jtm -Wall -std=gnu++14 -static -Ofast jtm.cpp
        sudo cp ./jtm /usr/local/bin/
        cd ..
    fi
    
    
    
}

build_deps

# Build every directory passed as parameter of the script
# E.g. ./build.sh hibench namenode
# The command above will build Dockerfiles inside ./hibench and ./namenode

build_all "-S" "hadoop"
